package com.example.spring_itadakimasu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringItadakimasuApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringItadakimasuApplication.class, args);
    }

}
